var keystone = require('keystone'),
    Types = keystone.Field.Types;

var CaseContent = new keystone.List('CaseContent', {
    autokey: { from: 'name', path: 'key' }
});

CaseContent.add({
    name: { type: String, required: true },
    text: {
        left: { type: Types.Html, wysiwyg: true, height: 100 },
        right: { type: Types.Html, wysiwyg: true, height: 100 }
    },
    gallery: { type: Types.CloudinaryImages }
});

//CaseHeadline.addPattern('standard meta');
CaseContent.register();
