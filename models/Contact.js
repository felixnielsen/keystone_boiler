var keystone = require('keystone'),
    Types = keystone.Field.Types;

var Contact = new keystone.List('Contact', {
    autokey: { from: 'name', path: 'key' }
});

Contact.add({
    name: { type: String, required: true },
    text: { type: Types.Html, wysiwyg: true, height: 300 }
});


Contact.register();
