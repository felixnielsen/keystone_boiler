var keystone = require('keystone'),
    Types = keystone.Field.Types;

var About = new keystone.List('About', {
    autokey: { from: 'name', path: 'key' }
});

About.add({
    name: { type: String, required: true },
    text: { type: Types.Html, wysiwyg: true, height: 300 }
});


About.register();
