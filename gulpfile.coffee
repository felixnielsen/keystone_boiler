# Load plugins
gulp = require 'gulp'
debug = require 'gulp-debug'
stylus = require 'gulp-stylus'
nib = require "nib"
rupture = require 'rupture'
flatten = require 'gulp-flatten'
minifycss = require 'gulp-minify-css'
coffee = require 'gulp-coffee'
uglify = require 'gulp-uglify'
gutil = require 'gulp-util'
changed = require 'gulp-changed'
imagemin = require 'gulp-imagemin'
ftp = require 'gulp-ftp'
rename = require 'gulp-rename'
clean = require 'gulp-clean'
concat = require 'gulp-concat'
replace = require 'gulp-replace'
notify = require 'gulp-notify'
livereload = require 'gulp-livereload'
lr = require 'tiny-lr'
server = lr()

# get package and config
pkg = require './package.json'

# define some paths, check config.json file
JS_ORDERED_FILES = pkg.projectFiles.scripts
CSS_ORDERED_FILES = pkg.projectFiles.styles

# path object
paths =
	coffee:
		src: 'src/scripts/**/*.coffee'
		dst: 'public/scripts'
	
	stylus:
		src: 'src/styles/**/*.styl'
		dst: 'public/styles'
	
	javascript:
		src: 'public/scripts/**/*.js'
		dst: 'public/scripts'
	
	stylesheet:
		src: 'public/styles/**/*.css'
		dst: 'public/styles'

	images: 
		src: 'src/images/**/*'
		dst: 'public/images'
	
	production:
		dst: 'public/assets'




# Styles
gulp.task 'styles', ->
	if CSS_ORDERED_FILES.length > 31
		throw new Error("Number of css files is over IE limit!");

	return gulp.src paths.stylus.src
	.pipe flatten()
	.pipe changed paths.stylus.dst,
		extension: '.css'
	.pipe stylus
		use: [nib(), rupture()]
		errors: true
	.on('error', gutil.log)
	.on('error', gutil.beep)
	.on("error", notify.onError({
		message: "Error: <%= error.message %>",
		title: "Error running styles"
	}))
	.pipe gulp.dest paths.stylus.dst
	.pipe livereload server
	.pipe notify
		icon: __dirname + "/gulp.png"
		title: "Skynet says styles"
		message: 'Styles task complete'

gulp.task 'styles-min', ['styles'], ->
	css = []
	for cssFileStr in CSS_ORDERED_FILES
		css.push cssFileStr.split("src/").join("public/")

	return gulp.src css
	.pipe concat 'main.css'
	.pipe rename
		suffix: '.min'
	.pipe minifycss()
	.on("error", notify.onError({
		message: "Error: <%= error.message %>",
		title: "Error running styles-min"
	}))
	.pipe gulp.dest paths.production.dst+"/styles"
	.pipe notify
		icon: __dirname + "/gulp.png"
		title: "Skynet says styles"
		message: 'Styles-min task complete'

# Scripts
gulp.task 'scripts', ->
	return gulp.src paths.coffee.src
	.pipe changed paths.coffee.dst,
		extension: '.js'
	.pipe coffee
		bare: true
	.on('error', gutil.log)
	.on('error', gutil.beep)
	.on("error", notify.onError({
		message: "Error: <%= error.message %>",
		title: "Error running scripts"
	}))
	.pipe gulp.dest paths.coffee.dst
	.pipe livereload server
	.pipe notify
		icon: __dirname + "/gulp.png"
		title: "Skynet says scripts"
		message: 'Scripts task complete'

gulp.task 'scripts-min', ['scripts'], ->
	js = []
	for jsFileStr in JS_ORDERED_FILES
		js.push jsFileStr.split("src/").join("public/")

	return gulp.src js
	.pipe concat 'main.js'
	.pipe rename
		suffix: '.min'
	.pipe uglify()
	.on("error", notify.onError({
		message: "Error: <%= error.message %>",
		title: "Error running scripts-min"
	}))
	.pipe gulp.dest paths.production.dst+"/scripts"
	.pipe notify
		icon: __dirname + "/gulp.png"
		title: "Skynet says scripts"
		message: 'Scripts-min task complete'

# copy the static js files over and keep their folder structure
gulp.task 'file-copy', ->
	#js files, static (vendors etc.) js files to the local folder
	gulp.src ['src/scripts/**/*', "!src/scripts/**/*.coffee"],
		base: 'src/scripts'
	.pipe gulp.dest './public/scripts/'

	#php files, static php files to the assets folder
	gulp.src 'src/php/**/*.php',
		base: 'src/php'
	.pipe gulp.dest './public/assets/php/'

	#svg files, static svg files to the assets folder
	return gulp.src 'src/svg/**/*.svg', 
		base: 'src/svg'
	.pipe gulp.dest './public/assets/svg/'
 
# Images
gulp.task 'images', ->
	return gulp.src [paths.images.src, '!src/images/{base64,base64/**}']
	.pipe changed paths.images.dst #Out-comment to let it run all the way through...
	.pipe imagemin
		optimizationLevel: 3
		progressive: true
		interlaced: true 
	.on("error", notify.onError({
		message: "Error: <%= error.message %>",
		title: "Error running images"
	}))
	.pipe livereload server
	.pipe gulp.dest paths.images.dst
	.pipe notify
		icon: __dirname + "/gulp.png"
		title: "Skynet says images"
		message: 'Images task complete'
 
# Clean
gulp.task 'clean', ->
	return gulp.src ['public/index.php', 'public/assets/*'],
		read: false
	.pipe clean
		force: true

# FTP
gulp.task 'ftp-deploy', ->
	return gulp.src ['public/**/*', '!public//**}']
	.pipe ftp
		host: 'web20.gigahost.dk'
		user: 'username'
		pass: 'password'
		remotePath: "/"
		exclude: "public"
	
	.pipe notify
		icon: __dirname + "/gulp.png"
		message: 'Upload complete'

#  //WORK FORCE TASKS
# Default task, compile assets
gulp.task 'default', -> console.log "no 'default' task, run 'gulp deploy/watch'"

# Run public evertime a new style or script file etc. is added to the static list of files.
gulp.task 'build', -> gulp.start 'file-copy', 'scripts', 'styles', 'images', 'watch'

# Deploy task, compiles assets to assets folder, and uploads. excludes
gulp.task 'deploy', -> gulp.start 'styles', 'scripts', 'scripts-min', 'styles-min', 'file-copy', 'scripts-min', 'images' #, 'ftp-deploy'
 
# Watch
gulp.task 'watch', ->
	console.log "\nWatch task started"
	# Listen on port 35729
	server.listen 35729, (err) ->
		return console.log "LIVE RELOAD ERROR:", err if err

		# Watch .styl files
		gulp.watch 'src/styles/**/*.styl', ['styles'] 

		# Watch .js files
		gulp.watch 'src/scripts/**/*.coffee', ['scripts']

		# Watch image files
		gulp.watch 'src/images/**/*', ['images']










# project installation/first-run
 #                  ___           ___                       ___                                 
 #    ___          /__/\         /  /\          ___        /  /\                                
 #   /  /\         \  \:\       /  /:/_        /  /\      /  /::\                               
 #  /  /:/          \  \:\     /  /:/ /\      /  /:/     /  /:/\:\    ___     ___   ___     ___ 
 # /__/::\      _____\__\:\   /  /:/ /::\    /  /:/     /  /:/~/::\  /__/\   /  /\ /__/\   /  /\
 # \__\/\:\__  /__/::::::::\ /__/:/ /:/\:\  /  /::\    /__/:/ /:/\:\ \  \:\ /  /:/ \  \:\ /  /:/
 #    \  \:\/\ \  \:\~~\~~\/ \  \:\/:/~/:/ /__/:/\:\   \  \:\/:/__\/  \  \:\  /:/   \  \:\  /:/ 
 #     \__\::/  \  \:\  ~~~   \  \::/ /:/  \__\/  \:\   \  \::/        \  \:\/:/     \  \:\/:/  
 #     /__/:/    \  \:\        \__\/ /:/        \  \:\   \  \:\         \  \::/       \  \::/   
 #     \__\/      \  \:\         /__/:/          \__\/    \  \:\         \__\/         \__\/    
 #                 \__\/         \__\/                     \__\/                                
# after a fork, run gulp install

prompt = require "gulp-prompt"
fs = require 'fs'

gulp.task 'install', ->
	return gulp.src('package.json')
	
	# set project name
	.pipe prompt.prompt
		type: 'input'
		name: 'name'
		default: "project"
		message: 'Name of project and database'
	, (res) ->
		_name = res.name.split(" ").join("")

		# update project name in package.json..
		file_content = fs.readFileSync './package.json'
		content = JSON.parse file_content
		content.name = _name
		fs.writeFileSync "./package.json", JSON.stringify(content, null, 4), 'utf-8' # stringify 3d paraemter keeps the whitespace tidy

		# update keystone.s file
		data = fs.readFileSync './keystone.js', 'utf-8'
		newValue = data.replace /KeyTest/gi, _name
		fs.writeFileSync './keystone.js', newValue, 'utf-8'
	
	# name space in package.json
	.pipe prompt.prompt
		type: 'input'
		name: 'namespace'
		default: "com.project"
		message: 'Project namespace, ex.: net.thezoomproject, gg.rwat, dk.joha'
	, (res) ->
		_ns = res.namespace.split(".")
		if _ns.length < 2
			throw new Error("Two name spaces not detected. use pattern, ex.: dk.pol")

		# rename actual folders
		fs.rename './src/scripts/com/domain', './src/scripts/'+_ns[0]+"/"+_ns[1], (err) ->
			if err then console.log('ERROR: ' + err)
		fs.rename './src/styles/com/domain', './src/styles/'+_ns[0]+"/"+_ns[1], (err) ->
			if err then console.log('ERROR: ' + err)

		# rename folders String refs
		file_content = fs.readFileSync './package.json'
		content = JSON.parse file_content

		# scripts
		for i in [0..content.projectFiles.scripts.length-1] by 1
			content.projectFiles.scripts[i] = content.projectFiles.scripts[i].split("com/domain").join(_ns[0]+"/"+_ns[1])
		
		# stringify 3d paraemter keeps the whitespace tidy
		fs.writeFileSync "./package.json", JSON.stringify(content, null, 4), 'utf-8'

	# notify on complete
	.pipe notify
		icon: __dirname + "/gulp.png"
		message: 'Project setup complete'



