#/*  ___
#  _/ ..\
# ( \  0/___
#  \    ___)
#  /     \
# /      _\
#`''''``
#author: felix nielsen<felix.nielsen@bacondeczar.com>*/#

class PagesCollection extends Backbone.Collection

	@MODEL_TYPES:
		"storelocator": PageModel

	routeRootURL: "/"
	url: ""
	ajaxObj: null

	$_context: null

	# types that are used to update a view only, like filtering
	# optional parameter for options
	_updateTypes: null

	# class instance reference
	# can be changed to something different than PageModel, if extending the PageModel is needed.
	_pageClassType: null

	# data transfer object
	_dto: null

	initialize : (options) ->
		# context of which we prepend the content to
		@$_context = options.$context

		# optionally, 
		@_pageClassType = if options.pageClass then options.pageClass else PageModel

		# optionally, add filter types, so we can differenciate from normal page types and page types that updates an excisting page..
		@_updateTypes = options.filterTypes if options.filterTypes

		#routing URL from Router, used in Fetch
		@routeRootURL = options.rootURL if options

	dealocModelsAndGetPrevious: ->
		# deactivate old model
		@model.setActiveTo false, @model if @model

		@model?.unbind()
		@model?.removeView()#will remove view it self.

		if @model
			prevModel = @model.clone()

			@model = null

			return prevModel
		else
			return null

	fetch : (page, type) ->
		@trigger "fetching"

		# create Data Transfer Object
		@_dto =
			type: type
			url: page

		log "PagesCollection, fetch:", page, type

		# @model = @checkIfModelExistWith page

		isFirstRender = window.initialPageJSON isnt null
		if isFirstRender
			@model = null

		
		actNormal = @actNormalUponType(type)
		callPageUpdate = actNormal # call page update, ex. the Filtering the Guidepage
		pageURL = ""
		isModelTheSameAsCurrent = @model.get("url") is page if @model

		if actNormal and !isModelTheSameAsCurrent
			# for when site is json only..
			# @setModelData model

			if isFirstRender
				# content is already in the dom the first time running the website
				@setModelData(window.initialPageJSON, true)
				delete window.initialPageJSON #clear it, as we don't need it anymore.
				window.initialPageJSON = null
			else
				# call the page, and get some json
				@makeAjaxCall()
				
		else
			@_dto.fragment = Backbone.history.fragment

			if !isModelTheSameAsCurrent
				@makeAjaxCall()
			else if !callPageUpdate and isModelTheSameAsCurrent
				@model.get("view").update(@_dto)
			else if callPageUpdate or isModelTheSameAsCurrent
				@model.get("view").update(@_dto)

	makeAjaxCall: (dto) ->
		# abort current ajax call if it is calling
		@ajaxObj?.abort()

		@ajaxObj = $.ajax
			url: @routeRootURL + @_dto.url
			dataType: 'json'
			success: relax.dom.bind(@, @onNewPageRequested)
			error: relax.dom.bind(@, @onPageRequestError)

	# overwrite this for specific type checking
	# type is set in the Router
	actNormalUponType: (type) -> if @_updateTypes then @_updateTypes.indexOf(type) is -1 else type isnt "update-only"

	setModelData: (json, firstTimeSet = false) ->
		model = new @_pageClassType
			"data": json

		# check if new page is current page..
		if @model and @model.get("url") is model.get("url")
			model.destroy()
			# page is the same, so prevent we go further in this method
			return null

		# disable old models
		prevModel = @dealocModelsAndGetPrevious()

		@model = model

		@model.once "ready", relax.dom.bind(@, @onModelReady)

		@model.set
			"context": @$_context
			"url": @_dto.url
			"type": @_dto.type
			"collection": @
			"first-time-set": firstTimeSet

		@model.set "prev-model", prevModel if prevModel
		
		#set the model to active
		@model.setActiveTo true

		@trigger "updated", @model

	onModelReady: -> @trigger "ready", @model

	checkIfModelExistWith: (page) ->
		foundModel = null
		_.each @models, (model) ->
			if model.get("url") is page
				foundModel = model

		return foundModel

	onNewPageRequested: (json, html, css) ->
		@ajaxObj = null

		#data should be JSON... overwritting for test purpose.
		#data = PageTestData #module?
		data = json.data

		#here we connect with the PagesView render
		@setModelData data

	onPageRequestError: (err) ->
		#catch error, and get the ID of the failing page.
		console.log("onPageRequestError:", err);



