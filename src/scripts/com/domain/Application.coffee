#      ___           ___           ___       ___           ___     
#     /\  \         /\  \         /\__\     /\  \         |\__\    
#    /::\  \       /::\  \       /:/  /    /::\  \        |:|  |   
#   /:/\:\  \     /:/\:\  \     /:/  /    /:/\:\  \       |:|  |   
#  /::\~\:\  \   /::\~\:\  \   /:/  /    /::\~\:\  \      |:|__|__ 
# /:/\:\ \:\__\ /:/\:\ \:\__\ /:/__/    /:/\:\ \:\__\ ____/::::\__\
# \/_|::\/:/  / \:\~\:\ \/__/ \:\  \    \/__\:\/:/  / \::::/~~/~   
#    |:|::/  /   \:\ \:\__\    \:\  \        \::/  /   ~~|:|~~|    
#    |:|\/__/     \:\ \/__/     \:\  \       /:/  /      |:|  |    
#    |:|  |        \:\__\        \:\__\     /:/  /       |:|  |    
#     \|__|         \/__/         \/__/     \/__/         \|__|    
#     ___
#   _/ ..\
#  ( \  0/___
#   \    \___)
#   /     \
#  /      _\
# `''''``       
# filename: application.coffee
# created: 06/06/2014
# author: felix nielsen <felix@flexmotion.com>
window.application =

	stageModel: null
	pages: null

	tracking: null

	initialize : ->
		@stageModel = new StageModel()

		@tracking = new GATracking()

		obj =
			"*": DefaultPage
		# 	"dictionary": DictionaryPage
		# 	"guide": GuidePage
		# 	"guide-detail": GuideDetailPage
		# 	"blog": DefaultPage
		
		# set the page models View Types
		PageModel.setPageTypes(obj)

		@pages = new PagesCollection
			filterTypes: ["cases-filter"]
			$context: $("#page-content")
			rootURL: "/"

		@router = new Router
			pages: @pages
	
		# start the whole thing
		@router.start()


		DebugMenu.init
			open: true
			pages: [
					url: "/"
					label: "home"
				,
					url: "/cases"
					label: "cases"
					children:[
							url: "/53e352976b6f1e6b6ae242fb"
							label: "test_case"
							children:[
								url: "/uhwe"
								label: "test_case_test"
							]
						,
							url: "/filter/mahogni,oak"
							label: "test_case_filter"
					]
				,
					url: "/about"
					label: "about"
				,
					url: "/contact"
					label: "contact"
			]

$ ->
	relax.setLog window.location.href.indexOf("localhost") is -1

	application.initialize()

	true
