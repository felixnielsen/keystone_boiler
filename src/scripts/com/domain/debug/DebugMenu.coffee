DebugMenu =
	enabled: false
	_el: null
	_open: true
	init: (options) ->
		#requires jQuery to work, throw error if jQuery is not present.
		throw new Error("DebugMenu debug requires jQuery to work") unless window.jQuery

		@enabled = true

		# listen to the page system
		window.application.pages.on "updated", @onPageUpdated, @

		str = "<b>RWATGG debug menu</b></br>" +
		"<br />Only use during development"

		@_el = $("body").prepend("<div id='DebugMenu-debug'><div style='line-height: 16px; padding: 4px; padding-left: 6px; background-color: #FCF8E3; color: #555555; display: block; margin-left: 10px; margin-top: 10px; width: 280px;'>" + str + "</div>").find("#DebugMenu-debug")

		@buildTree options.pages, @_el
		@addStyle()

		if options.open
			$("#DebugMenu-debug").addClass "open"

	addStyle: ->
		css = "html,body{width:100%;height:100%;}"+
				"#DebugMenu-debug{background:#e7e7e7;color:black;font-family:Georgia, Verdana;font-size:11px; line-height:11px;padding-right:10px;box-shadow:6px 7px 8px -4px #e7e7e7;overflow:auto;width:0;height:100%;position:fixed;z-index:9999999999999999;top:0;left:0;transition:width .5s cubic-bezier(0.190,1.000,0.220,1.000);-moz-transition:width .5s cubic-bezier(0.190,1.000,0.220,1.000);-webkit-transition:width .5s cubic-bezier(0.190,1.000,0.220,1.000);-o-transition:width .5s cubic-bezier(0.190,1.000,0.220,1.000);margin:0;}"+
				"#DebugMenu-debug * {box-sizing: border-box;}"+
				"#DebugMenu-debug.open{width:300px;}"+
				"#DebugMenu-debug ul{overflow:hidden;margin:0;padding:0 0 0 15px;}"+
				"#DebugMenu-debug > ul{margin-top:10px;margin-bottom:10px;padding:0 0 0 10px;}"+
				"#DebugMenu-debug ul li{border:1px solid #fafafa;cursor:pointer;opacity:1;list-style:none;background:white;width:300px;transition:margin .25s cubic-bezier(0.190,1.000,0.220,1.000);-moz-transition:margin .25s cubic-bezier(0.190,1.000,0.220,1.000);-webkit-transition:margin .25s cubic-bezier(0.190,1.000,0.220,1.000);-o-transition:margin .25s cubic-bezier(0.190,1.000,0.220,1.000);margin:0;padding:7px 2px 7px 6px;}"+
				"#DebugMenu-debug ul li.selected:before{content:'> ';}"+
				"#DebugMenu-debug ul li.selected{background:#000000;color:#ffffff;margin:10px 0;}"+
				"#DebugMenu-debug ul li.prev-selected{background:#e9e9e9;}"+
				"#DebugMenu-debug ul li:hover{opacity:.7;}";

		head = document.getElementsByTagName("head")[0]
		style = document.createElement("style")

		style.type = "text/css"
		if style.styleSheet
			style.styleSheet.cssText = css
		else
			style.appendChild document.createTextNode(css)

		head.appendChild style

		#if mobile or tablet
		#open the menu always, else add mousemove.
		$(document).bind "mousemove.debug", relax.dom.bind(this, @onMouseMove)

	onMouseMove: (event) ->
		cur = @_open
		@_open = event.pageX <= ((if @_open then 250 else 40))
		unless cur is @_open
			if @_open
				$("#DebugMenu-debug").addClass "open"
			else
				$("#DebugMenu-debug").removeClass "open"

	buildTree: (json, el, trailingURL = null) ->
		for node in json
			id = node.url.split("/").join("-")
			container = el.append("<ul id='" + id + "-wrapper'></ul>").find("ul#" + id + "-wrapper")
			url = if trailingURL then trailingURL + node.url else node.url
			url = url.split("//").join("/")
			log url
			new DebugMenu.DebugBtn(url, container.append("<li id='" + id + "-btn'><span>" + node.label + "</span></li>").find("#" + id + "-btn"))

			if node.children and node.children.length > 0
				@buildTree node.children, container, url

	onPageUpdated: (model) ->
		log "onPageUpdate", model
		# @_el.find("li").removeClass "prev-selected"
		# @_el.find("li").removeClass "selected"
		# @_el.find("li#" + previousNode.id + "-btn").addClass "prev-selected"  if previousNode
		# @_el.find("li#" + currentNode.id + "-btn").addClass "selected"

DebugMenu.DebugBtn = Class.extend
	_url: ""
	_el: null
	init: (url, el) ->
		@_url = url
		@_el = el
		@_el.click relax.dom.bind(this, @onClick)

	onClick: (event) -> window.application.router.navigate(@_url)
