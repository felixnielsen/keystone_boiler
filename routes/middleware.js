/**
 * This file contains the common middleware used by your routes.
 * 
 * Extend or replace these functions as your application requires.
 * 
 * This structure is not enforced, and just a starting point. If
 * you have more middleware you may want to group it as separate
 * modules in your project's /lib directory.
 */

var _ = require('underscore'),
	querystring = require('querystring'),
	general = require('./general/generals'),
	keystone = require('keystone'),
	nodeCache = require( "node-cache" );
	
// application cache object, notice it is created without the var keyword, which makes it global
cache = new nodeCache( { stdTTL: 3600, checkperiod: 0  } );


/**
	Initialises the standard view locals
*/

exports.initLocals = function(req, res, next) {
	
	var locals = res.locals;

	locals.user = req.user;
	locals.isAdmin = req.session.userId != undefined; //could properly be a better check.... gonna learn!

	// make the menu available via the locals object
	// locals.menu = ->
	general.setMenu(req, res, next);
};


/**
	Fetches and clears the flashMessages before a view is rendered
*/

exports.flashMessages = function(req, res, next) {
	
	var flashMessages = {
		info: req.flash('info'),
		success: req.flash('success'),
		warning: req.flash('warning'),
		error: req.flash('error')
	};
	
	res.locals.messages = _.any(flashMessages, function(msgs) { return msgs.length; }) ? flashMessages : false;
	
	next();
	
};


/**
	Prevents people from accessing protected pages when they're not signed in
 */

exports.requireUser = function(req, res, next) {
	
	if (!req.user) {
		req.flash('error', 'Please sign in to access this page.');
		res.redirect('/keystone/signin');
	} else {
		next();
	}
	
};

/**
	parse the initial page's JSON, to be used in the front-end
	the pageJSON is printed into the layout.ejs file
*/
exports.addGlobalJSON = function(req, res, next) {
	res.locals.pageJSON = JSON.stringify(res.locals.pageData);
	console.log(res.locals.routes)
	next();
}
