var keystone = require('keystone');

exports.setMenu = function(req, res, next) {

	function setLocalsMenu(menu)
	{
		res.locals.menu = menu;
	};

	// check if the menu is in the cache
    if(Object.getOwnPropertyNames(cache.get("menu")).length > 0)
    {
    	// set locals menu
    	setLocalsMenu(cache.get("menu").menu);
    	
    	// continue in the app
    	next();

    	// prevent further logic
        return;
    }

	// generate menu, as it is not in the cache
	var cases,
		about,
		contact,
		locals = res.locals,
		find = locals.isAdmin ? {} : { published: true };

	var menu = {
		"cases": undefined,
		"about": undefined,
		"contact": undefined
	};

	var checkQuerie = function()
	{
		if(menu["cases"] !== undefined && menu["about"] !== undefined && menu["contact"] !== undefined)
		{
			// set menu in the cache
		    cache.set("menu", menu);
		    
		    // set locals menu
		    setLocalsMenu(cache.get("menu").menu);

		    // continue in the app
			next();
		}
	};

	keystone.list('Cases')
		.model
		.find(find)
		.where("offline", "false")
		.populate('content')
		.populate('type')
		.exec(function(err, data) {
			menu["cases"] = data;
			checkQuerie();
	});

	keystone.list('About').model.find().exec(function(err, data) {
		menu["about"] = data;
		checkQuerie();
	});

	keystone.list('Contact').model.find().exec(function(err, data) {
		menu["contact"] = data;
		checkQuerie();
	});

	return;
};
