/**
 * This file is where you define your application routes and controllers.
 * 
 * Start by including the middleware you want to run for every request;
 * you can attach middleware to the pre('routes') and pre('render') events.
 * 
 * For simplicity, the default setup for route controllers is for each to be
 * in its own file, and we import all the files in the /routes/views directory.
 * 
 * Each of these files is a route controller, and is responsible for all the
 * processing that needs to happen for the route (e.g. loading data, handling
 * form submissions, rendering the view template, etc).
 * 
 * Bind each route pattern your application should respond to in the function
 * that is exported from this module, following the examples below.
 * 
 * See the Express application routing documentation for more information:
 * http://expressjs.com/api.html#app.VERB
 */

var _ = require('underscore'),
	keystone = require('keystone'),
	middleware = require('./middleware'),
	importRoutes = keystone.importer(__dirname);

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

// Import Route Controllers
var routes = {
	views: importRoutes('./views')
};

// route URLs
var ROUTES = {
	root: "/",
	about: "/about",
	contact: "/contact",
	cases: "/cases",
	aCase: "/cases/:case",
	casesFilter: "/cases/filter/:type"
};

// 404
keystone.set('404', function(req, res, next) {
	res.status(404).render('errors/404');
});

// 500
keystone.set('500', function(req, res, next) {
	// TODO: Send off an e-mail or drop something into Google Analytics
	res.status(500).render('errors/500');
});

// Setup Route Bindings
exports = module.exports = function(app) {
	// routes, set them in locals, so we can expose the routes to the front-end
	app.locals.routes = JSON.stringify(ROUTES);

	// Views
	// design pattern: use the get middlelayer for communicating with mongo, if view does not need to communicate with mongo, then just exlcude the get method.
	app.get(ROUTES.root, routes.views.index.get, middleware.addGlobalJSON, routes.views.index);
	app.get(ROUTES.about, routes.views.index.get, middleware.addGlobalJSON, routes.views.index);
	app.get(ROUTES.contact, routes.views.contact.get, middleware.addGlobalJSON, routes.views.contact);
    app.get(ROUTES.cases, routes.views.cases.get, middleware.addGlobalJSON, routes.views.cases);
    app.get(ROUTES.aCase, routes.views.cases.get, middleware.addGlobalJSON, routes.views.cases);
    app.get(ROUTES.casesFilter, routes.views.cases.get, middleware.addGlobalJSON, routes.views.cases);
	
	// NOTE: To protect a route so that only admins can see it, use the requireUser middleware:
	// app.get('/protected', middleware.requireUser, routes.views.protected);
	
};
