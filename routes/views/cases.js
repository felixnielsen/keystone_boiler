var keystone = require('keystone');
var Case = keystone.list('Case');
var CaseContent = keystone.list('CaseContent');
var mongoose = keystone.mongoose;


exports = module.exports = function(req, res) {
	var view = new keystone.View(req, res),
		locals = res.locals;

	if(req.xhr) {
        return res.json({
            cases: locals.pageData
        });
    } else {
    	// view.query('cases', data);
        return view.render("partials/templates/cases", {
            cases: locals.pageData
        });
    };
};

// get the data from mongo
exports.get = module.exports.get = function(req, res, next) {
    var locals = res.locals,
    	find = locals.isAdmin ? {} : { published: true };

    keystone.list('Cases')
        .model
        .find(find)
        .where("offline", "false")
        .populate('content')
        .populate('type')
        .exec(function(err, data) {
        	locals.pageData = data;
        	next();
        });
};
