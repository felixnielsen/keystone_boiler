var keystone = require('keystone');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res),
		locals = res.locals;

	locals.section = "Contact"
	locals.title = "Contact"

	if(req.xhr) {
        return res.json({
            data: locals.pageData
        });
    } else {
    	// view.query('cases', data);
        return view.render("partials/templates/contact", {
        	data: locals.pageData   
        });
    };
	
};

exports.get = module.exports.get = function(req, res, next) {
	var locals = res.locals;
	locals.pageData = {};
	next();
};
